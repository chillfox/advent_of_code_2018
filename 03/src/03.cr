require "matrix"

max_width = 0
max_height = 0
claims = Array(Regex::MatchData | Nil).new
File.each_line ARGV[0] do |line|
  #  id, _, position, size = line.split ' '
  claim = line.match /(?<id>#\d+)\s@\s(?<left>\d+),(?<top>\d+):\s(?<wide>\d+)x(?<tall>\d+)/
  width = claim.not_nil!["left"].to_i + claim.not_nil!["wide"].to_i
  height = claim.not_nil!["top"].to_i + claim.not_nil!["tall"].to_i

  if width > max_width
    max_width = width
  end
  if height > max_height
    max_height = height
  end

  claims << claim
end

fabric = Matrix.new(max_height, max_width) { |idx, row, col| 0 }
# top, left
# fabric[0, 2] = 1

claims.each do |claim|
  top = claim.not_nil!["top"].to_i
  left = claim.not_nil!["left"].to_i
  wide = claim.not_nil!["wide"].to_i
  tall = claim.not_nil!["tall"].to_i

  cut = Matrix.new(max_height, max_width) do |idx, row, col|
    if col >= left && row >= top && col < left + wide && row < top + tall
      1
    else
      0
    end
  end
  # puts cut
  fabric += cut

end

puts "Fabric dimensions: #{fabric.dimensions}"
# puts "Fabric claims"
# puts fabric
puts "Overlapping pices: #{fabric.to_a.count { |i| i > 1 }}"

claims.each do |claim|
  top = claim.not_nil!["top"].to_i
  left = claim.not_nil!["left"].to_i
  wide = claim.not_nil!["wide"].to_i
  tall = claim.not_nil!["tall"].to_i

  # start_row, rows, start_col, columns
  cut = fabric.minor top, tall, left, wide
  if cut.size == cut.to_a.sum
    id = claim.not_nil!["id"].to_s
    puts "Uniq claim: #{id}"
    break
  end
end
