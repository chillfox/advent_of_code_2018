
REGEX = /\[(?<year>\d+)\-(?<month>\d+)\-(?<day>\d+)\s(?<hour>\d+)\:(?<minute>\d+)\]\s(?<note>.*)/

guard_roster = Array(NamedTuple(time: Time, note: String)).new
File.each_line ARGV[0] do |line|
  match = line.match REGEX
  # pp match
  time = Time.new(
    match.not_nil!["year"].to_i,
    match.not_nil!["month"].to_i,
    match.not_nil!["day"].to_i,
    match.not_nil!["hour"].to_i,
    match.not_nil!["minute"].to_i
    )
  # p time
  guard_roster << { time: time, note: match.not_nil!["note"].to_s }
end
guard_roster.sort_by! { |event| event[:time] }
# pp guard_roster

guards = Hash(Int32, Hash(Symbol, Array(Int32))).new

guard_on_duty = 0
guard_falls_asleep = Time.now
guard_roster.each do |event|
  # p event
  case event[:note]
  when "falls asleep"
    guard_falls_asleep = event[:time]
    # puts "Guard ##{guard_on_duty} falls asleep"
  when "wakes up"
    time_asleep = event[:time] - guard_falls_asleep
    if guards.has_key? guard_on_duty
      guards[guard_on_duty][:minutes_asleep] << time_asleep.minutes
    else
      sleepy_minutes = Array.new 60, 0
      # sleepy_minutes[guard_falls_asleep.minute..event[:time].minute].each { |m| m += 1 }
      guards[guard_on_duty] = {:minutes_asleep => [time_asleep.minutes], :sleepy_minutes => sleepy_minutes }
      # p sleepy_minutes
    end
    guards[guard_on_duty][:sleepy_minutes].each_index do |minute|
      if minute >= guard_falls_asleep.minute && minute < event[:time].minute
        guards[guard_on_duty][:sleepy_minutes][minute] += 1
      end
    end
    # puts "Guard ##{guard_on_duty} wakes up"
  else
    guard_on_duty = event[:note].match(/(\d+)/).not_nil![0].to_i
    # puts "Guard ##{guard_on_duty} begins shift"
  end
end

# p guards

# guards.each do |guard|
#   p guard.keys
# end

puts "Strategy 1"
guard = guards.to_a.sort_by { |g| g.last[:minutes_asleep].sum } .last
puts "Most sleepy guard ##{guard.first}"

# minute = guard.last[:sleepy_minutes].sort.last
# p guard.last[:sleepy_minutes].map_with_index { |m, i| p i }

repeat = guard.last[:sleepy_minutes].sort.last
minute = 0
guard.last[:sleepy_minutes].each_index do |i|
  if guard.last[:sleepy_minutes][i] == repeat
    minute = i
  end
end
puts "Most sleepy minute #{minute}"

puts "#{guard.first} * #{minute} = #{guard.first * minute}"


puts "Strategy 2"
guard = guards.to_a.sort_by { |g| g.last[:sleepy_minutes].sort.last } .last
puts "Most regular sleeper ##{guard.first}"

repeat = guard.last[:sleepy_minutes].sort.last
minute = 0
guard.last[:sleepy_minutes].each_index do |i|
  if guard.last[:sleepy_minutes][i] == repeat
    minute = i
  end
end
puts "Most sleepy minute #{minute}"

puts "#{guard.first} * #{minute} = #{guard.first * minute}"
