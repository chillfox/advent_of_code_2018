

two_letters = 0
three_letters = 0
# ids = Array(String).new
File.each_line ARGV[0] do |line|
  # puts "ID: #{line}"
  uniq_chars = line.chars.uniq
  if line.size > uniq_chars.size
    # puts uniq_chars
    count_two = false
    count_three = false
    uniq_chars.each do |char|
      count = line.count char
      # puts "Char: #{char}, Count: #{count}"
      if count == 2
        count_two = true
        # two_letters += 1
        # puts "Char: #{char}, Count: #{count}"
      elsif count == 3
        count_three = true
        # three_letters += 1
        # puts "Char: #{char}, Count: #{count}"
      end
    end
    if count_two
      two_letters += 1
      # ids.push line
    end
    if count_three
      three_letters += 1
      # ids.push line
    end
  end
end

checksum = two_letters * three_letters
puts "Two letters (#{two_letters}) * three letters (#{three_letters}) = checksum (#{checksum})"


ids = File.read ARGV[0]
ids.each_line do |line1|
  # puts "ID: #{line1}"
  id_found = false
  ids.each_line do |line2|
    similar_chars = 0
    diff_index = 0
    line1.chars.each_with_index do |char, index|
      if char == line2[index]
        similar_chars += 1
      else
        diff_index = index
      end
    end
    if similar_chars == line1.size - 1
      common_chars = line1.sub diff_index, ""
      puts "Common letters: #{common_chars}"
      id_found = true
      break
    end
  end
  break if id_found
end
