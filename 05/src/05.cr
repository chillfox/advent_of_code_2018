
data = File.read(ARGV[0]).chomp

def react(polymers : String)
  polymers.each_char_with_index do |polymer, index|
    next if index == 0
    next unless polymer.downcase == polymers[index - 1].downcase
    next unless polymer.lowercase? && polymers[index - 1].uppercase? || polymer.uppercase? && polymers[index - 1].lowercase?
    # puts "polymer '#{polymer}' at index ##{index}"
    return react polymers.sub((index -1)..index, "")
  end
  polymers
end

reaction = react data
puts "Polymers left after reaction: #{reaction.size}"

shortest_reaction = reaction.size
26.times do |n|
  # p n
  n += 97
  # puts n.chr
  current_reaction = react data.gsub(n.chr, "").gsub(n.chr.upcase, "")
  if current_reaction.size < shortest_reaction
    shortest_reaction = current_reaction.size
  end
end
puts "The shortest reaction is: #{shortest_reaction}"
